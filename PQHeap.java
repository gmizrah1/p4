/** Gabriela Mizrahi-Arnaud, Erez Krimsky, Ben Gellman.
 *  gmizrah1, ekrimsk1, bgelma1
 *  Data Structures Section 2
 *  Project 4
 *  This class implements the PriorityQueue interface as a heap.
 */
import java.util.ArrayList;
import java.util.Collection;

public class PQHeap<T extends Comparable<? super T>> implements PriorityQueue<T> {

    /**ArrayList to hold heap structure.*/
    private ArrayList<T> heap;
    /** Number of elements in the array, maybe dont need this.*/
    private int size;

    public PQHeap() {
    }
    
    public void insert(T t) {
        //add t to end of the heap
        heap.add(t);
        //bubble up to ensure validity of heap property

    }

    public  void remove() throws QueueEmptyException {

    }
 
      public T peek() throws QueueEmptyException {
        if (this.isEmpty() ) {
    //        throw new QueueEmptyException();
        }
        return this.heap.get(1);
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public void clear() {
        this.heap = new ArrayList<T>();
        this.size = 0;
    }
    
     public void init(Collection<T> values) {}


}
